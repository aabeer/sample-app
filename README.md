== README

* Ruby version 4.2.2

* Database creation PostgreSQL

* How to run the test suite

Install rails v.4.2.2 : gem install rails -v 4.2.4

Clone the project : git pull https://aabeer@bitbucket.org/aabeer/sample-app.git

Migrate: buncle exec rake db:migrate

Keep developing on branches if you want: git checkout -b newlocalbranchname origin/branchname

<tt>rake doc:app</tt>.