class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
    	# Redirect the user to the page
    	log_in user
    	params[:session][:remember_me] == '1' ? rember(user) : forget(user)
    	redirect_to user_url(user)
	    else
    	# Show the errors
    	flash.now[:danger] = "Incorrect cretantials"
    	render 'new'
    end
  end

  def destroy
	log_out if logged_in?
  	redirect_to root_url
  end
end
